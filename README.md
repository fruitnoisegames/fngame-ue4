# Word Lab: Letter Arsenal [Rogue-like]

## Current Dependencies:

[Unreal Engine 4, Version 4.17](https://www.unrealengine.com)

-----------------------

## SETUP:

### Prerequisites:
	
[Visual Studio 2015](https://www.visualstudio.com/en-us/downloads/download-visual-studio-vs.aspx) is required for Unreal Engine 4 to compile. 
Visual Studio 2013 will _not_ work, so please make sure you download the correct version.
**Make sure you install all of the Windows SDKs when installing (Windows 8/8.1 and Windows 10)**

Also, this is currently only tested on **Windows 7**, **Windows 8** and **Windows 10**.

### Steps:

1. Download Unreal Engine 4. Run the launcher and log in. Once logged in, go to _Library_ and install version **4.17** of the Unreal Engine. Make sure the **debug symbols** option is checked when downloading.

2. Go to your git repo managing program and download this repo to **%userprofile%\Documents\Unreal Projects\FNGame**.

3. Open the project in Unreal Engine 4.

4. Go to `File > Refresh Visual Studio Project` and wait for it to finish working.

5. Press the `Compile` button above the viewport, and wait for it to finish working.

6. In `Edit > Editor Preferences...`, Under `Experimental`, enable **Environmental Query System**. Also, under `Loading & Saving`, Enable **Force Compilation on Startup**. Finally, make sure that under `Blueprint Editor`, **Save on Compile** is set to either _On Success Only_ or _Always_.

7. Everything should be ready to go.