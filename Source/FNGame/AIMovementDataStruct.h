// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "Engine/DataTable.h"
#include "Engine/UserDefinedStruct.h"
#include "AIMovementDataStruct.generated.h"

USTRUCT(BlueprintType)
struct FAIMovementDataStruct : public FTableRowBase {
		GENERATED_USTRUCT_BODY()
	public:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovementDataStruct")
		TArray<FVector> points;
};
