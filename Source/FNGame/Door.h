// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "WallBase.h"
#include "Door.generated.h"

UENUM(BlueprintType)
namespace DoorDirection
{
	enum Type {
		North UMETA(DisplayName = "North"),
		South UMETA(DisplayName = "South"),
		East UMETA(DisplayName = "East"),
		West UMETA(DisplayName = "West")
	};
}

UCLASS()
class FNGAME_API ADoor : public AWallBase
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADoor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<DoorDirection::Type> direction;
	
};
