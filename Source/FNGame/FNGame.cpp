// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "FNGame.h"


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, FNGame, "FNGame" );

DEFINE_LOG_CATEGORY(LogFNGame)
 