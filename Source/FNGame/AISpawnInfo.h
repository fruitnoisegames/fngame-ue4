// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/UserDefinedStruct.h"
#include "AISpawnInfo.generated.h"

class ACharacterStats;

USTRUCT(BlueprintType)
struct FAISpawnInfo
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AISpawnInfo")
		float enemyDifficulty;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AISpawnInfo")
		TSubclassOf<ACharacterStats> ai;
};

