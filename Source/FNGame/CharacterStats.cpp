// Fill out your copyright notice in the Description page of Project Settings.

#include "FNGame.h"
#include "AISpawnInfo.h"
#include "AIDataLibraryEntry.h"
#include "CharacterStats.h"


void ACharacterStats::TakeDamage(AActor * source, float damage, bool ignoreDefense)
{
	//calculate damage
	if (ignoreDefense)
	{
		stats.CurrentHP = FMath::Max(stats.CurrentHP - damage,0.0f);
	}
	else
	{
		stats.CurrentHP = FMath::Max(stats.CurrentHP + FMath::Min(stats.Defense - damage,0.0f),0.0f);
	}

	//die
	if (stats.CurrentHP == 0)
	{
		OnCharacterDeath(source);
	}
}

bool ACharacterStats::CharacterAttack()
{
	float gameTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	float deltaTime = gameTime - attackTime;

	if (deltaTime >= stats.AttackSpeed)
	{
		attackTime = gameTime;
		return true;
	}
	return false;
}

void ACharacterStats::OnCharacterDeath_Implementation(AActor * source)
{
	isDead = true;
	GetMesh()->SetSimulatePhysics(true);
}

// Sets default values
ACharacterStats::ACharacterStats()
{
	isDead = false;
	damageAfterWeakness = 0.0f;
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	previousRoom = NULL;

}

// Called when the game starts or when spawned
void ACharacterStats::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACharacterStats::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ACharacterStats::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

