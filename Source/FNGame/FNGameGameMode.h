 // Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "FNGameGameMode.generated.h"

UCLASS(minimalapi)
class AFNGameGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AFNGameGameMode();
};



