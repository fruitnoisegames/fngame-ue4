// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/UserDefinedStruct.h"
#include "CharacterStat.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FCharacterStat
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStat")
		float CurrentHP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStat")
		float AttackPower;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStat")
		float Defense;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStat")
		float AttackSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStat")
		float MovementSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStat")
		float MaxHealth;
};
