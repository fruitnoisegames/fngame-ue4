// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "ProjectInfoLibrary.generated.h"

/**
 * 
 */
UCLASS()
class FNGAME_API UProjectInfoLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Project Information")
		static FString GetProjectVersion();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Project Information")
		static FString GetProjectTitle();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Project Information")
		static FString GetCompanyName();
	
};
