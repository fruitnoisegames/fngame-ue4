// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "KeybindingFunctionLibrary.generated.h"

/**
 * Library to change keybindings.
 */

//Custom Action Mapping Struct
USTRUCT(BlueprintType)
struct FFNGInputMapping
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
		FName ActionName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
		FKey Key;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
		FString KeyAsString;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
		uint32 bShift : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
		uint32 bCtrl : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
		uint32 bAlt : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
		uint32 bCmd : 1;


	FFNGInputMapping() {}
	FFNGInputMapping(const FName InActionName, const FKey InKey, const bool bInShift, const bool bInCtrl, const bool bInAlt, const bool bInCmd)
		: Key(InKey)
		, KeyAsString(Key.GetDisplayName().ToString())
		, bShift(bInShift)
		, bCtrl(bInCtrl)
		, bAlt(bInAlt)
		, bCmd(bInCmd)
	{
		ActionName = InActionName;
	}

};

//Custom Axis Mapping Struct
USTRUCT(BlueprintType)
struct FFNGAxisMapping
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
		FName AxisName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
		FKey Key;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
		FString KeyAsString;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
		float Scale = 1.0;



	FFNGAxisMapping() {}
	FFNGAxisMapping(const FName InAxisName, const FKey InKey, const float InScale)
		: Key(InKey)
		, KeyAsString(Key.GetDisplayName().ToString())
		, Scale(InScale)
	{
		AxisName = InAxisName;
	}

};

UCLASS()
class FNGAME_API UKeybindingFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category = "Keybindings")
		static bool RebindKey(FFNGInputMapping Action);
	UFUNCTION(BlueprintCallable, Category = "Keybindings")
		static bool RebindAxis(FFNGAxisMapping Axis);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Keybindings")
		static FFNGInputMapping GetKeybindingByName(FName ActionName);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Keybindings")
		static FFNGInputMapping GetKeybindingByKey(FKey ActionKey);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Keybindings")
		static TArray<FFNGAxisMapping> GetAxisBindingByName(FName AxisName);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Keybindings")
		static FFNGAxisMapping GetAxisBindingByKey(FKey AxisKey);
	
	
};