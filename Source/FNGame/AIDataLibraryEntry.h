// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataTable.h"
#include "Engine/UserDefinedStruct.h"
#include "AIDataLibraryEntry.generated.h"

class ACharacterStats;
/**
 * 
 */
USTRUCT(BlueprintType)
struct FAIDataLibraryEntry : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AIEntry")
		TSubclassOf<ACharacterStats> ai;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AIEntry")
		float chance;

};
