// Fill out your copyright notice in the Description page of Project Settings.
#include "FNGame.h"
#include "TileRoom.h"
#include "FNGameInstance.h"
#include "AISpawner.h"

void UAISpawner::SpawnEnemies(TArray<ATileRoom*> rooms, UDataTable* ai, UCurveFloat* enemyAmount, UCurveFloat* enemyDifficulty, int32 level, int seed) {
	
	static FMath Math;
	Math.RandInit(seed);

	//number of enemies
	int32 enemyQty = (int32) enemyAmount->GetFloatValue(level);
	int32 numEnemies = enemyQty;

	int32 totalWeight = 0;
	TArray<FAIDataLibraryEntry*> entries = SortAIEntries(ai,totalWeight);

	for (int32 r = 0; r < rooms.Num(); r++) {
		enemyQty = numEnemies;
		//50/50 chance of adding or subtracting
		//+/-2 because of 3
		if (Math.Rand() % 2 == 1) {
			enemyQty += Math.Rand() % 3;
		} 
		else{ 
			enemyQty -= Math.Rand() % 3;
		}

		for (int32 n = 0; n < enemyQty; n++){
			//enemy difficulty, random increase by a small bit for randomization +0 to +2
			float difficulty = (float)(enemyDifficulty->GetFloatValue(level) + Math.Rand() % 3);

			//opposing randomization, -0 to -2
			difficulty -= (float)(Math.Rand() % 3);

			//negative not allowed
			if (difficulty <= 0.0f){
				difficulty = 1.0f;
			}

			//balance the enemy level based on the number of enemies, more enemies = no boost, less = bigger boost
			//if ((float)(difficulty - enemyQty) > 0 && n == enemyQty - 1){
			//	difficulty += 2.0f;
			//}

			//Select which enemy to spawn
			//number generator from 0 to totalWeight - 1

			int32 randResult = Math.RandRange(0, (int32)totalWeight - 1);
			int32 total = 0;

			//AISpawnInfo initialization
			FAISpawnInfo newAI;
			newAI.enemyDifficulty = difficulty;

			//Generate AI information here
			for (int32 k = entries.Num()-1; k >= 0; k--){
				total += entries[k]->chance;
				if (total > randResult){
					newAI.ai = entries[k]->ai;
					break;
				}
			}
			rooms[r]->CacheAIToSpawn(newAI);
		}
	}
}

//Sort Data entries from largest chance to smallest chance
TArray<FAIDataLibraryEntry*> UAISpawner::SortAIEntries(UDataTable* table, int32 &total){
	TArray<FAIDataLibraryEntry*> info;

	//iterate each row and add to array
	for (auto it : table->RowMap){
		FAIDataLibraryEntry* entry = (FAIDataLibraryEntry*)(it.Value);
		total += entry->chance;
		info.Add(entry);
	}

	//sort array ascending order
	int32 j = 0;
	FAIDataLibraryEntry* temp = 0;

	for (int32 i = 1; i < info.Num(); i++){
		j = i;

		while (j > 0 && info[j] < info[j - 1]){
			temp = info[j];
			info[j] = info[j - 1];
			info[j - 1] = temp;
			j--;
		}
	}
	return info;
}
