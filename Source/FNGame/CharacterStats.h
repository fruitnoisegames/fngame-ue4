// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TileRoom.h"
#include "CharacterStat.h"
#include "GameFramework/Character.h"
#include "BaseTrap.h"
#include "CharacterStats.generated.h"

struct FAISpawnInfo;
struct FAIDataLibrary;

UENUM(BlueprintType)
namespace ElementType
{
	enum Type {
		None UMETA(DisplayName = "None"),
		Fire UMETA(DisplayName = "Fire"),
		Water UMETA(DisplayName = "Water"),
		Electric UMETA(DisplayName = "Electric"),
		Poison UMETA(DisplayName = "Poison"),
		Ice UMETA(DisplayName = "Ice"),
		Earth UMETA(DisplayName = "Earth"),
		Wind UMETA(DisplayName = "Wind"),
		Snake UMETA(DisplayName = "Snaaaaake"),
		Acid UMETA(DisplayName = "Acid"),
		Sticky UMETA(DisplayName = "Sticky")
	};
}

UENUM(BlueprintType)
namespace StatusEffect
{
	enum Type {
		None UMETA(DisplayName = "None"),
		Burn UMETA(DisplayName = "Burn"),
		Freeze UMETA(DisplayName = "Freeze"),
		Poison UMETA(DisplayName = "Poison"),
		Stun UMETA(DisplayName = "Stun")
	};
}

USTRUCT(BlueprintType)
struct FStatusEffectStruct
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effect")
		TEnumAsByte<StatusEffect::Type> Status;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effect")
		float duration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effect")
		float DPS;

	FStatusEffectStruct() {}
};

UCLASS()
class FNGAME_API ACharacterStats : public ACharacter
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	bool fire;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	bool water;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	bool electric;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	bool poison;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	bool ice;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	bool earth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	bool wind;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	bool snaaaaake;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	bool acid;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	bool sticky;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	bool isAlly;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	bool overlap;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	float damageMult = 1.0f;

	UPROPERTY(BlueprintReadOnly)
	bool isDead;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ATileRoom* currentRoom;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ATileRoom* previousRoom;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float difficultyModifier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ElementType::Type> Element;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	FCharacterStat stats;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	FCharacterStat defaultStats;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	float attackTime;

	//VARIABLE SCOPE IS LIMITED TO CHARACTER TAKE DAMAGE
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterStats")
	float damageAfterWeakness;

	UFUNCTION(BlueprintCallable, Category = "CharacterStats")
	void TakeDamage(AActor* source, float damage, bool ignoreDefense);

	UFUNCTION(BlueprintCallable, Category = "CharacterStats")
	bool CharacterAttack();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "CharacterStats")
	void CauseStatusEffects(FStatusEffectStruct effect, ABaseTrap* trap);
	
	UFUNCTION(BlueprintNativeEvent, Category = "CharacterStats")
	void OnCharacterDeath(AActor* source);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "CharacterStats")
	void CharacterTakeDamage(AActor* source, float damage, bool ignoreDefense);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "CharacterStats")
	void CharacterChangeRoom(ACharacterStats* source);

	// Sets default values for this character's properties
	ACharacterStats();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
};
