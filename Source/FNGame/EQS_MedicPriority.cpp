// Fill out your copyright notice in the Description page of Project Settings.

#include "FNGame.h"
#include "EQS_MedicPriority.h"

void UEQS_MedicPriority::RunTest(FEnvQueryInstance & QueryInstance) const
{
	for (FEnvQueryInstance::ItemIterator It(this, QueryInstance); It; ++It)
	{
		ACharacterStats* character = Cast<ACharacterStats>(GetItemActor(QueryInstance, It));
		if (character)
		{
			float weight = 1 - (character->stats.CurrentHP / character->stats.MaxHealth);
			It.SetScore(TestPurpose, FilterType, (character->isDead) ? 0.0f : weight, FloatValueMin.GetValue(), FloatValueMax.GetValue());
		}
	}
}

UEQS_MedicPriority::UEQS_MedicPriority()
{
	ValidItemType = UEnvQueryItemType_ActorBase::StaticClass();
}



