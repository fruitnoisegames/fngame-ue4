// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "Kismet/BluePrintFunctionLibrary.h"
#include "Runtime/Engine/Classes/Engine/DataTable.h"
#include "AIDataLibraryEntry.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTree.h"
#include "Runtime/AIModule/Classes/Blueprint/AIBlueprintHelperLibrary.h"
#include "AISpawner.generated.h"

class ATileRoom;

UCLASS()
class FNGAME_API UAISpawner : public UBlueprintFunctionLibrary
{

	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "AISpawner")
		static void SpawnEnemies(TArray<ATileRoom*> rooms, UDataTable* ai, UCurveFloat* enemyAmount, UCurveFloat* enemyDifficulty, int32 level, int seed);

private:
		static TArray<FAIDataLibraryEntry*> SortAIEntries(UDataTable* table,int32 &total);
};
