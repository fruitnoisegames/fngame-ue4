// Fill out your copyright notice in the Description page of Project Settings.

#include "FNGame.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerInput.h"
#include "Runtime/Engine/Classes/GameFramework/InputSettings.h"
#include "KeybindingFunctionLibrary.h"

bool UKeybindingFunctionLibrary::RebindKey(FFNGInputMapping Action)
{
	UInputSettings* Settings = GetMutableDefault<UInputSettings>();
	if (!Settings) return false;

	TArray<FInputActionKeyMapping>& Actions = Settings->ActionMappings;

	for (int i = 0; i < Actions.Num(); i++)
	{
		FInputActionKeyMapping& act = Actions[i];
		if (act.ActionName == Action.ActionName)
		{
			//Change the action mapping to the new one
				act.Key = Action.Key;
				act.bShift = Action.bShift;
				act.bCtrl = Action.bCtrl;
				act.bAlt = Action.bAlt;
				act.bCmd = Action.bCmd;


			//Save the keybinds to Input.ini
			const_cast<UInputSettings*>(Settings)->SaveKeyMappings();

			//And rebuild it
			for (TObjectIterator<UPlayerInput> It; It; ++It)
			{
				It->ForceRebuildingKeyMaps(true);
			}
			return true;
		}
	}

	return false;

}

bool UKeybindingFunctionLibrary::RebindAxis(FFNGAxisMapping Axis)
{
	UInputSettings* Settings = GetMutableDefault<UInputSettings>();
	if (!Settings) return false;

	TArray<FInputAxisKeyMapping>& Axes = Settings->AxisMappings;

	for (int i = 0; i < Axes.Num(); i++)
	{
		FInputAxisKeyMapping& ax = Axes[i];
		if (ax.AxisName == Axis.AxisName)
		{
			if(ax.Scale==Axis.Scale)
			{
				//Change the action mapping to the new one
				ax.Key = Axis.Key;
				//Save the keybinds to Input.ini
				const_cast<UInputSettings*>(Settings)->SaveKeyMappings();

				//And rebuild it
				for (TObjectIterator<UPlayerInput> It; It; ++It)
				{
					It->ForceRebuildingKeyMaps(true);
				}
				return true;
			}
		}
	}

	return false;
}

FFNGInputMapping UKeybindingFunctionLibrary::GetKeybindingByName(FName ActionName) {

	FFNGInputMapping inputmap;
	UInputSettings* Settings = GetMutableDefault<UInputSettings>();
	if (!Settings) return inputmap;

	TArray<FInputActionKeyMapping>& Actions = Settings->ActionMappings;

	for (int i = 0; i < Actions.Num(); i++)
	{
		FInputActionKeyMapping& act = Actions[i];
		if (act.ActionName == ActionName)
		{
			inputmap.Key = act.Key;
			inputmap.bShift = act.bShift;
			inputmap.bCtrl = act.bCtrl;
			inputmap.bAlt = act.bAlt;
			inputmap.bCmd = act.bCmd;
			inputmap.KeyAsString = inputmap.Key.GetDisplayName().ToString();
		}
	}
	return inputmap;

}

FFNGInputMapping UKeybindingFunctionLibrary::GetKeybindingByKey(FKey ActionKey)
{
	FFNGInputMapping inputmap;
	UInputSettings* Settings = GetMutableDefault<UInputSettings>();
	if (!Settings) return inputmap;

	TArray<FInputActionKeyMapping>& Actions = Settings->ActionMappings;

	for (int i = 0; i < Actions.Num(); i++)
	{
		FInputActionKeyMapping& act = Actions[i];
		if (act.Key== ActionKey)
		{
			inputmap.Key = act.Key;
			inputmap.bShift = act.bShift;
			inputmap.bCtrl = act.bCtrl;
			inputmap.bAlt = act.bAlt;
			inputmap.bCmd = act.bCmd;
			inputmap.KeyAsString = inputmap.Key.GetDisplayName().ToString();
		}
	}


	return inputmap;
}

TArray<FFNGAxisMapping> UKeybindingFunctionLibrary::GetAxisBindingByName(FName AxisName)
{

	TArray<FFNGAxisMapping> inputmap;
	UInputSettings* Settings = GetMutableDefault<UInputSettings>();
	if (!Settings) return inputmap;

	TArray<FInputAxisKeyMapping>& Axes = Settings->AxisMappings;

	FFNGAxisMapping inputtmp;
	for (int i = 0; i < Axes.Num(); i++)
	{
		FInputAxisKeyMapping& ax = Axes[i];
		if (ax.AxisName == AxisName)
		{
			inputtmp.Key = ax.Key;
			inputtmp.KeyAsString = inputtmp.Key.GetDisplayName().ToString();
			inputtmp.Scale = ax.Scale;
			inputmap.Add(FFNGAxisMapping(inputtmp));
		}
	}
	return inputmap;
}

FFNGAxisMapping UKeybindingFunctionLibrary::GetAxisBindingByKey(FKey AxisKey)
{
	FFNGAxisMapping inputmap;
	UInputSettings* Settings = GetMutableDefault<UInputSettings>();
	if (!Settings) return inputmap;

	TArray<FInputAxisKeyMapping>& Axes = Settings->AxisMappings;

	for (int i = 0; i < Axes.Num(); i++)
	{
		FInputAxisKeyMapping& ax = Axes[i];
		if (ax.Key == AxisKey)
		{
			inputmap.Key = ax.Key;
			inputmap.KeyAsString = inputmap.Key.GetDisplayName().ToString();
			inputmap.Scale = ax.Scale;
		}
	}
	return inputmap;
}
