// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "FNGameCharacter.h"
#include "RoomObstacle.h"
#include "Wall.h"
#include "Door.h"
#include "GenerateMap.generated.h"

class ATileRoom;

DECLARE_MULTICAST_DELEGATE(DoorController)

UCLASS()
class FNGAME_API AGenerateMap : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AGenerateMap();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Dimensions")
		int32 length;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Dimensions")
		int32 width;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Dimensions")
		FVector roomSize;
	UPROPERTY(EditAnywhere, Category = "Blueprints")
		TSubclassOf<ATileRoom> tile;
	UPROPERTY(EditAnywhere, Category = "Blueprints")
		TSubclassOf<AWall> wall;
	UPROPERTY(EditAnywhere, Category = "Blueprints")
		TSubclassOf<ADoor> door;
	UPROPERTY(EditAnywhere, BlueprintReadOnly ,Category = "Blueprints")
		TArray<TSubclassOf<ARoomObstacle>> obstacles;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Current Tiles")
		TArray<ATileRoom*> tileList;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Current Walls")
		TArray<AWall*> wallList;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Current Doors")
		TArray<ADoor*> doorList;

	UPROPERTY(EditAnywhere, Category = "Minigames")
		int32 minigameSpawnSuccessChance;

	UFUNCTION(BlueprintCallable, Category = "Generate Map")
		void startGeneration(FVector bounds, int seed);
	UFUNCTION(BlueprintCallable, Category = "Generate Map")
		TArray<ATileRoom*> getTilesWithAttribute(Room::Type attribute);
	UFUNCTION(BlueprintCallable, Category = "Generate Map")
		ATileRoom* getTile(int32 row, int32 column);
	UFUNCTION(BlueprintCallable, Category = "Generate Map")
		void spawnRoomObstacle(ATileRoom* parent);

	DoorController controller;

private:

	UWorld *world;
	FMath Math;


	void generateTiles(FVector bounds);
	void setTileReferences();
	void setupWalls(FVector roomPlacement);
	void generateMaze();
	void traverse(ATileRoom* origin, ATileRoom* start);
	void markCriticalPath();
	void assignRoomObstacle(ATileRoom * current, ATileRoom * previous);
	void spawnObstacles();
	TArray<ATileRoom*> removeAdjacencies(TArray<ATileRoom*> subset, Room::Type attribute);
	void generateShopTile(int tilesWithin);
	void generateMinigamesTile(int tilesWithin);
	TArray<ATileRoom*> getTilesWithAttribute(Path::Type attribute);
};
