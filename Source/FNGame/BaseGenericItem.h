// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"
#include "BaseGenericItem.generated.h"

UENUM(BlueprintType)
namespace ItemType
{
	enum Type {
		None UMETA(DisplayName = "None"),
		Instant UMETA(DisplayName = "Instant"),
		Weapon UMETA(DisplayName = "Weapon"),
		Equipment UMETA(DisplayName = "Equipment"),
		Letter UMETA(DisplayName = "Letter"),
		Upgrade UMETA(DisplayName = "Upgrade"),
		Secondary UMETA(DisplayName = "Secondary"),
		Consumable UMETA(DisplayName = "Consumable")
	};
}

UENUM(BlueprintType)
namespace CharacterStatType
{
	enum Type {
		Health UMETA(DisplayName = "Health"),
		Attack UMETA(DisplayName = "Attack"),
		Defense UMETA(DisplayName = "Defense"),
		AttackSpeed UMETA(DisplayName = "AttackSpeed"),
		MovementSpeed UMETA(DisplayName = "MovementSpeed")
	};
}

UENUM(BlueprintType)
namespace ModifierType
{
	enum Type {
		Percent UMETA(DisplayName = "Percent"),
		Fixed UMETA(DisplayName = "Fixed")
	};
}

USTRUCT(BlueprintType)
struct FStatModifier
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<CharacterStatType::Type> stat;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<ModifierType::Type> modifier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float amount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool negative;
};

UCLASS()
class FNGAME_API ABaseGenericItem : public AActor
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Generic Item Property")
		UTexture2D *pickupImage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Generic Item Property")
		FString displayName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Generic Item Property")
		bool shopItem;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Generic Item Property")
		int32 itemCost;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Generic Item Property")
		int32 rawItemCost;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Generic Item Property")
		TEnumAsByte<ItemType::Type> itemType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Generic Item Property")
		FString Desc;

	// Sets default values for this actor's properties
	ABaseGenericItem();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Generic Item Function")
		void ActivateItem();
};
