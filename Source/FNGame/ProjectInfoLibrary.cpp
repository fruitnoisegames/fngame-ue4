// Fill out your copyright notice in the Description page of Project Settings.

#include "FNGame.h"
#include "ProjectInfoLibrary.h"

FString UProjectInfoLibrary::GetProjectVersion() {
	FString result;
	GConfig->GetString(
		TEXT("/Script/EngineSettings.GeneralProjectSettings"),
		TEXT("ProjectVersion"),
		result,
		GGameIni
		);
	return result;

}

FString UProjectInfoLibrary::GetProjectTitle() {
	FString result;
	GConfig->GetString(
		TEXT("/Script/EngineSettings.GeneralProjectSettings"),
		TEXT("ProjectDisplayedTitle"),
		result,
		GGameIni
		);
	return result;

}

FString UProjectInfoLibrary::GetCompanyName() {
	FString result;
	GConfig->GetString(
		TEXT("/Script/EngineSettings.GeneralProjectSettings"),
		TEXT("CompanyName"),
		result,
		GGameIni
		);
	return result;

}

