// Fill out your copyright notice in the Description page of Project Settings.

#include "FNGame.h"
#include "BaseGenericItem.h"


// Sets default values
ABaseGenericItem::ABaseGenericItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	rawItemCost = 0;

}

// Called when the game starts or when spawned
void ABaseGenericItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseGenericItem::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

