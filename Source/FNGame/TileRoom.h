// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "AISpawnInfo.h"
#include "TileRoom.generated.h"

class ARoomObstacle;
class UAISpawner;

UENUM(BlueprintType)
namespace Path
{
	enum Type {
		None UMETA(DisplayName = "None"),
		CriticalPath UMETA(DisplayName = "Critical Path"),
		DeadEnd UMETA(DisplayName = "Dead End"),
		Start UMETA(DisplayName = "Start"),
		Finish UMETA(DisplayName = "Finish")
	};
}

UENUM(BlueprintType)
namespace Room
{
	enum Type {
		None UMETA(DisplayName = "None"),
		Boss UMETA(DisplayName = "Boss"),
		Spawn UMETA(DisplayName = "Spawn"),
		Shop UMETA(DisplayName = "Shop"),
		Minigames UMETA(DisplayName = "Minigames")
	};
}

class AWallBase;
class AGenerateMap;

UCLASS()
class FNGAME_API ATileRoom : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATileRoom();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Add to precache AI
	void CacheAIToSpawn(FAISpawnInfo ai);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UBoxComponent* bounds;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		AGenerateMap* source;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FAISpawnInfo> aiToSpawn;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<Path::Type> pathType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<Room::Type> roomType;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 id;
	UPROPERTY(EditAnywhere,BlueprintReadOnly)
		int32 row;
	UPROPERTY(EditAnywhere,BlueprintReadOnly)
		int32 column;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool areDoorsClosed;
	UPROPERTY(EditAnywhere)
		ATileRoom* north;
	UPROPERTY(EditAnywhere)
		ATileRoom* south;
	UPROPERTY(EditAnywhere)
		ATileRoom* east;
	UPROPERTY(EditAnywhere)
		ATileRoom* west;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		AWallBase* northWall;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		AWallBase* southWall;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		AWallBase* eastWall;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		AWallBase* westWall;
	UPROPERTY(EditAnywhere)
		bool visited;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		ATileRoom* previous;
	UPROPERTY(EditAnywhere,BlueprintReadOnly)
		TArray<ATileRoom*> next;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AActor*> roomContents;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 obstacleID;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		ARoomObstacle* obstacle;
	UFUNCTION(BlueprintNativeEvent, Category = "Tileroom")
		void ComponentOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Tileroom")
		void LevelInitialized();
};
