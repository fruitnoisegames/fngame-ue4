// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CharacterStats.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_ActorBase.h"
#include "EnvironmentQuery/EnvQueryTest.h"
#include "EQS_MedicPriority.generated.h"

/**
*
*/
UCLASS()
class FNGAME_API UEQS_MedicPriority : public UEnvQueryTest
{
	GENERATED_BODY()

	virtual void RunTest(FEnvQueryInstance& QueryInstance) const override;
	UEQS_MedicPriority();



};
