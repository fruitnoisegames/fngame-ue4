// Fill out your copyright notice in the Description page of Project Settings.

#include "FNGame.h"
#include "GenerateMap.h"
#include "TileRoom.h"

/*NOTE: Tile Room Obstacle Array
0: Boss/Spawn
1: Minigames
2: Shop
3+: None/Dead End
*/
// Sets default values

AGenerateMap::AGenerateMap()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	roomSize = FVector::ZeroVector;
}

void AGenerateMap::startGeneration(FVector bounds, int seed)
{
	Math.RandInit(seed);
	world = GetWorld(); //is the world null?
	if (!world) return;
	roomSize.X = bounds.X / length;
	roomSize.Y = bounds.Y / width;
	generateTiles(bounds);
	setTileReferences();
	generateMaze();
	setupWalls(FVector(bounds.X/length,bounds.Y/width,0));
	markCriticalPath();
	generateShopTile(2);
	generateMinigamesTile(2);
	spawnObstacles();
	controller.Broadcast();
}

// Called when the game starts or when spawned
void AGenerateMap::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AGenerateMap::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGenerateMap::generateTiles(FVector bounds)
{
	int32 id = 0;
	for (int32 l = 0; l < length; l++)
	{
		for (int32 w = 0; w < width; w++)
		{
			ATileRoom *room = world->SpawnActor<ATileRoom>(tile, FVector(bounds.X*(2 * l + 1) / length, bounds.Y*(2 * w + 1) / width, 0), FRotator::ZeroRotator);
			room->source = this;
			room->column = l;
			room->row = w;
			room->visited = false;
			room->id = id++;
			room->north = NULL;
			room->south = NULL;
			room->east = NULL;
			room->west = NULL;
			room->pathType = Path::Type::None;
			room->roomType = Room::Type::None;
			tileList.Add(room);

			controller.AddUObject(room, &ATileRoom::LevelInitialized);
		}
	}

}

void AGenerateMap::setTileReferences()
{
	for (ATileRoom* var : tileList)
	{
		int32 west_ = var->column + 1;
		int32 east_ = var->column - 1;
		int32 north_ = var->row + 1;
		int32 south_ = var->row - 1;

		if (west_ >= 0) //West exists, row const
		{
			var->west = getTile(var->row, west_);
		}

		if (east_ <= length) //east exists, row const
		{
			var->east = getTile(var->row, east_);
		}

		if (north_ <= width) //north exists, col const
		{
			var->north = getTile(north_, var->column);
		}

		if (south_ >= 0) //south exists, col const
		{
			var->south = getTile(south_, var->column);
		}
	}
}

ATileRoom* AGenerateMap::getTile(int32 row, int32 column)
{
	for (ATileRoom *iter : tileList)
	{
		if (iter->column == column && iter->row == row)
		{
			return iter;
		}
	}
	return NULL;
}

void AGenerateMap::setupWalls(FVector location)
{

	FVector wallOrigin;
	FVector wallroomPlacement;
	float max = 500;

	for (ATileRoom* room : tileList)
	{

		if (!room->northWall) {//Execute code only if the wall doesn't exist
							   //Is there a room to the north and don't spawn the door/wall if there already exists a reference (Door/Wall exists already)
			if (room->north) //-1,1,0
			{
				//does the room to the north have any reference to the current tile?
				if (room->next.Contains(room->north) || room->previous == room->north) //Has a reference, use a door
				{
					ADoor* door_ = world->SpawnActor<ADoor>(door, FVector(-1 * location.X, location.Y, 0), FRotator::ZeroRotator);

					door_->GetActorBounds(false, wallOrigin, wallroomPlacement);
					door_->SetActorScale3D(FVector(location.X / wallroomPlacement.X, 1, (wallroomPlacement.Z >= max) ? 1 : max / wallroomPlacement.Z));
					door_->AttachRootComponentToActor(room);
					door_->reference = room;

					room->roomContents.Add(door_);
					doorList.Add(door_);

					room->northWall = door_;
					room->north->southWall = door_;
				}
				//no reference, use a wall
				else
				{
					AWall* wall_ = world->SpawnActor<AWall>(wall, FVector(-1 * location.X, location.Y, 0), FRotator::ZeroRotator);

					wall_->GetActorBounds(false, wallOrigin, wallroomPlacement);
					wall_->SetActorScale3D(FVector(location.X / wallroomPlacement.X, 1, (wallroomPlacement.Z >= max) ? 1 : max / wallroomPlacement.Z));
					wall_->AttachRootComponentToActor(room);
					wall_->reference = room;

					room->roomContents.Add(wall_);
					wallList.Add(wall_);

					room->north->southWall = wall_;
					room->northWall = wall_;
				}
			}
			//The room to the north doesn't exist, place a wall instead
			else
			{
				AWall* wall_ = world->SpawnActor<AWall>(wall, FVector(-1 * location.X, location.Y, 0), FRotator::ZeroRotator);

				wall_->GetActorBounds(false, wallOrigin, wallroomPlacement);
				wall_->SetActorScale3D(FVector(location.X / wallroomPlacement.X, 1, (wallroomPlacement.Z >= max) ? 1 : max / wallroomPlacement.Z));
				wall_->AttachRootComponentToActor(room);
				wall_->reference = room;

				room->roomContents.Add(wall_);
				wallList.Add(wall_);

				room->northWall = wall_;
			}
		}

		if (!room->eastWall) {
			if (room->east) //-1,-1, 0
			{
				if (room->next.Contains(room->east) || room->previous == room->east)
				{
					ADoor* door_ = world->SpawnActor<ADoor>(door, FVector(-1 * location.X, -1 * location.Y, 0), FRotator(0, 90, 0));

					door_->GetActorBounds(false, wallOrigin, wallroomPlacement);
					door_->SetActorScale3D(FVector(location.Y / wallroomPlacement.Y, 1, (wallroomPlacement.Z >= max) ? 1 : max / wallroomPlacement.Z));
					door_->AttachRootComponentToActor(room);
					door_->reference = room;

					room->roomContents.Add(door_);
					doorList.Add(door_);

					room->east->westWall = door_;
					room->eastWall = door_;

				}
				else
				{
					AWall* wall_ = world->SpawnActor<AWall>(wall, FVector(-1 * location.X, -1 * location.Y, 0), FRotator(0, 90, 0));

					wall_->GetActorBounds(false, wallOrigin, wallroomPlacement);
					wall_->SetActorScale3D(FVector(location.Y / wallroomPlacement.Y, 1, (wallroomPlacement.Z >= max) ? 1 : max / wallroomPlacement.Z));
					wall_->AttachRootComponentToActor(room);
					wall_->reference = room;

					room->roomContents.Add(wall_);
					wallList.Add(wall_);

					room->east->westWall = wall_;
					room->eastWall = wall_;
				}
			}
			else
			{
				AWall* wall_ = world->SpawnActor<AWall>(wall, FVector(-1 * location.X, -1 * location.Y, 0), FRotator(0, 90, 0));

				wall_->GetActorBounds(false, wallOrigin, wallroomPlacement);
				wall_->SetActorScale3D(FVector(location.Y / wallroomPlacement.Y, 1, (wallroomPlacement.Z >= max) ? 1 : max / wallroomPlacement.Z));
				wall_->AttachRootComponentToActor(room);
				wall_->reference = room;

				room->roomContents.Add(wall_);
				wallList.Add(wall_);

				room->eastWall = wall_;
			}
		}

		if (!room->westWall) {
			if (room->west) //1, -1, 0
			{
				if (room->next.Contains(room->west) || room->previous == room->west)
				{
					ADoor* door_ = world->SpawnActor<ADoor>(door, FVector(location.X, -1 * location.Y, 0), FRotator(0, 90, 0));

					door_->GetActorBounds(false, wallOrigin, wallroomPlacement);
					door_->SetActorScale3D(FVector(location.Y / wallroomPlacement.Y, 1, (wallroomPlacement.Z >= max) ? 1 : max / wallroomPlacement.Z));
					door_->AttachRootComponentToActor(room);
					door_->reference = room;

					room->roomContents.Add(door_);
					doorList.Add(door_);

					room->west->eastWall = door_;
					room->westWall = door_;
				}
				else
				{
					AWall* wall_ = world->SpawnActor<AWall>(wall, FVector(location.X, -1 * location.Y, 0), FRotator(0, 90, 0));

					wall_->GetActorBounds(false, wallOrigin, wallroomPlacement);
					wall_->SetActorScale3D(FVector(location.Y / wallroomPlacement.Y, 1, (wallroomPlacement.Z >= max) ? 1 : max / wallroomPlacement.Z));
					wall_->AttachRootComponentToActor(room);
					wall_->reference = room;

					room->roomContents.Add(wall_);
					wallList.Add(wall_);

					room->west->eastWall = wall_;
					room->westWall = wall_;
				}
			}
			else
			{
				AWall* wall_ = world->SpawnActor<AWall>(wall, FVector(location.X, -1 * location.Y, 0), FRotator(0, 90, 0));

				wall_->GetActorBounds(false, wallOrigin, wallroomPlacement);
				wall_->SetActorScale3D(FVector(location.Y / wallroomPlacement.Y, 1, (wallroomPlacement.Z >= max) ? 1 : max / wallroomPlacement.Z));
				wall_->AttachRootComponentToActor(room);
				wall_->reference = room;

				room->roomContents.Add(wall_);
				wallList.Add(wall_);

				room->westWall = wall_;
			}
		}

		if (!room->southWall) {
			if (room->south) //-1, -1, 0
			{
				if (room->next.Contains(room->south) || room->previous == room->south)
				{
					ADoor* door_ = world->SpawnActor<ADoor>(door, FVector(-1 * location.X, -1 * location.Y, 0), FRotator::ZeroRotator);

					door_->GetActorBounds(false, wallOrigin, wallroomPlacement);
					door_->SetActorScale3D(FVector(location.X / wallroomPlacement.X, 1, (wallroomPlacement.Z >= max) ? 1 : max / wallroomPlacement.Z));
					door_->AttachRootComponentToActor(room);
					door_->reference = room;

					room->roomContents.Add(door_);
					doorList.Add(door_);

					room->southWall = door_;
					room->south->northWall = door_;
				}
				else
				{
					AWall* wall_ = world->SpawnActor<AWall>(wall, FVector(-1 * location.X, -1 * location.Y, 0), FRotator::ZeroRotator);

					wall_->GetActorBounds(false, wallOrigin, wallroomPlacement);
					wall_->SetActorScale3D(FVector(location.X / wallroomPlacement.X, 1, (wallroomPlacement.Z >= max) ? 1 : max / wallroomPlacement.Z));
					wall_->AttachRootComponentToActor(room);
					wall_->reference = room;

					room->roomContents.Add(wall_);
					wallList.Add(wall_);

					room->south->northWall = wall_;
					room->southWall = wall_;
				}
			}
			else
			{
				AWall* wall_ = world->SpawnActor<AWall>(wall, FVector(-1 * location.X, -1 * location.Y, 0), FRotator::ZeroRotator);

				wall_->GetActorBounds(false, wallOrigin, wallroomPlacement);
				wall_->SetActorScale3D(FVector(location.X / wallroomPlacement.X, 1, (wallroomPlacement.Z >= max) ? 1 : max / wallroomPlacement.Z));
				wall_->AttachRootComponentToActor(room);
				wall_->reference = room;

				room->roomContents.Add(wall_);
				wallList.Add(wall_);

				room->southWall = wall_;
			}
		}
	}
}

void AGenerateMap::generateMaze()
{
	//Start with a random tile
	ATileRoom* selected = tileList[Math.RandRange(0, tileList.Num() - 1)];
	selected->visited = true;
	selected->pathType = Path::Type::Start;
	selected->roomType = Room::Type::Spawn;
	selected->obstacleID = 0;
	ACharacter* pChar = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	pChar->SetActorLocation(selected->GetActorLocation() + FVector(0,0,200));
	traverse(selected, selected);
}

void AGenerateMap::traverse(ATileRoom* origin, ATileRoom* start)
{
	TArray<ATileRoom*> available;

	//Check for null and if visited. will add to candidency to what is selectable by random generator if both are true
	if (origin->north && !origin->north->visited)
	{
		available.Add(origin->north);
	}
	if (origin->south && !origin->south->visited)
	{
		available.Add(origin->south);
	}
	if (origin->east && !origin->east->visited)
	{
		available.Add(origin->east);
	}
	if (origin->west && !origin->west->visited)
	{
		available.Add(origin->west);
	}

	if (available.Num() > 0) //Available nodes
	{
		ATileRoom *next_ = available[Math.RandRange(0, available.Num() - 1)];
		next_->previous = origin;
		origin->next.Add(next_);
		assignRoomObstacle(next_, origin);
		next_->visited = true;
		traverse(next_, start);
	}
	else if (available.Num() == 0) //Dead End
	{
		if (origin == start) return; //Start marker will never be a dead end since it was assigned already
		else
		{
			if (origin->next.Num() <= 0)
			{
				origin->pathType = Path::Type::DeadEnd;
				assignRoomObstacle(origin, origin->previous);
			}
			traverse(origin->previous, start);
		}
	}
}

//Returns an array of tiles that have the specified path tile attribute
TArray<ATileRoom*> AGenerateMap::getTilesWithAttribute(Path::Type attribute)
{
	TArray<ATileRoom*> results;

	for (ATileRoom* tile : tileList)
	{
		if (tile->pathType == attribute)
		{
			results.Add(tile);
		}
	}
	return results;
}
//Returns an array of tiles that have the specified room tile attribute
TArray<ATileRoom*> AGenerateMap::getTilesWithAttribute(Room::Type attribute)
{
	TArray<ATileRoom*> results;

	for (ATileRoom* tile : tileList)
	{
		if (tile->roomType == attribute)
		{
			results.Add(tile);
		}
	}
	return results;
}

void AGenerateMap::markCriticalPath()
{
	TArray<ATileRoom*> endpoints = getTilesWithAttribute(Path::Type::DeadEnd);

	//There is no dead ends
	if (endpoints.Num() <= 0) return;

	ATileRoom* end = endpoints[Math.RandRange(0, endpoints.Num() - 1)];
	end->pathType = Path::Type::Finish;
	end->roomType = Room::Type::Boss;
	end->obstacleID = 0;
	end = end->previous;
	
	while (end->pathType != Path::Type::Start)
	{
		end->pathType = Path::Type::CriticalPath;
		end = end->previous;
	}
}

void AGenerateMap::assignRoomObstacle(ATileRoom* current, ATileRoom * previous)
{
	TArray<int32> slots;
	for (int k = 1; k < obstacles.Num(); k++)
	{
		slots.Add(k);
	}
	slots.Remove(previous->obstacleID);
	int32 num = Math.RandRange(3, slots.Num() - 1);
	if (num >= 3)
	{
		current->obstacleID = slots[num];
	}
	else //only happens when the array size is less than three
	{
		current->obstacleID = slots[0];
	}
}

void AGenerateMap::spawnRoomObstacle(ATileRoom * parent)
{
	float maxHeight = 500;
	float scaleMaxHeight = 0.75;
	FVector origin;
	FVector bounds;
	ARoomObstacle* obs = world->SpawnActor<ARoomObstacle>(obstacles[parent->obstacleID],FVector(0.0,0.0,0.0),FRotator::ZeroRotator);
	obs->GetActorBounds(false, origin, bounds);
	obs->SetActorScale3D(FVector(roomSize.X / bounds.X, roomSize.Y / bounds.Y, (parent->obstacleID > 2) ? maxHeight*scaleMaxHeight / bounds.Z : 1));
	obs->AttachToActor(parent, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, true));
	parent->obstacle = obs;
	obs->parent = parent;
}

void AGenerateMap::spawnObstacles()
{
	for (ATileRoom* room : tileList)
	{
		spawnRoomObstacle(room);
	}
}

//Removes rooms for candidency.
TArray<ATileRoom*> AGenerateMap::removeAdjacencies(TArray<ATileRoom*> subset, Room::Type attribute)
{
	ATileRoom* center = getTilesWithAttribute(attribute)[0];
	for (ATileRoom* adjacent : center->next)
	{
		subset.Remove(adjacent);
	}
	subset.Remove(center->previous);
	subset.Remove(center);
	return subset;
}

void AGenerateMap::generateShopTile(int tilesWithin)
{
	//Assume greater than 2x2
	if (tileList.Num() <= 4) return;
	TArray<ATileRoom*> tileSubSet(tileList); //I dont want to mess with the original copy of the TArray
	
	//Remove Boss Room from possible room candidates
	ATileRoom* center = getTilesWithAttribute(Room::Type::Boss)[0];
	tileSubSet.Remove(center);

	//Room cannot exist within two rooms of spawn
	ATileRoom* cent = getTilesWithAttribute(Room::Type::Spawn)[0];
	tileSubSet.Remove(cent);

	for (int32 r = -1*tilesWithin; r < tilesWithin+1; r++)
	{
		int32 localRow = cent->row;
	    localRow += r;
		
		for (int32 c = -1*tilesWithin; c < tilesWithin+1; c++)
		{
			int32 localColumn = cent->column;
			if (localRow == 0 && localColumn == 0)
			{
				continue;
			}
			localColumn += c;

			//Check for existence
			ATileRoom* target = getTile(localRow, localColumn);
			if(target != NULL)
			{
				//Remove
				tileSubSet.Remove(target);
			}
		}
	}

	ATileRoom* shop = tileSubSet[Math.RandRange(0, tileSubSet.Num() - 1)];
	shop->roomType = Room::Shop;
	shop->obstacleID = 2;
}

void AGenerateMap::generateMinigamesTile(int tilesWithin)
{
	//Assume greater than 2x2
	if (tileList.Num() <= 4) return;

	int32 value = Math.RandRange(0, 100);
	if (value < minigameSpawnSuccessChance) return; //failed

	TArray<ATileRoom*> tileSubSet(tileList); //I dont want to mess with the original copy of the TArray

	//Remove shop tile for candidate
	tileSubSet.Remove(getTilesWithAttribute(Room::Type::Shop)[0]);

	//Room cannot exist within two rooms of spawn
	ATileRoom* cent = getTilesWithAttribute(Room::Type::Spawn)[0];
	tileSubSet.Remove(cent);

	for (int32 r = -1 * tilesWithin; r < tilesWithin + 1; r++)
	{
		int32 localRow = cent->row;
		localRow += r;

		for (int32 c = -1 * tilesWithin; c < tilesWithin + 1; c++)
		{
			int32 localColumn = cent->column;
			if (localRow == 0 && localColumn == 0)
			{
				continue;
			}
			localColumn += c;

			//Check for existence
			ATileRoom* target = getTile(localRow, localColumn);
			if (target != NULL)
			{
				//Remove
				tileSubSet.Remove(target);
			}
		}
	}

	//Room cannot exist within two rooms of spawn
	ATileRoom* boss = getTilesWithAttribute(Room::Type::Boss)[0];
	tileSubSet.Remove(boss);

	for (int32 r = -1 * tilesWithin; r < tilesWithin + 1; r++)
	{
		int32 localRow = boss->row;
		localRow += r;

		for (int32 c = -1 * tilesWithin; c < tilesWithin + 1; c++)
		{
			int32 localColumn = boss->column;
			if (localRow == 0 && localColumn == 0)
			{
				continue;
			}
			localColumn += c;

			//Check for existence
			ATileRoom* target = getTile(localRow, localColumn);
			if (target != NULL)
			{
				//Remove
				tileSubSet.Remove(target);
			}
		}
	}

	ATileRoom* shop = tileSubSet[Math.RandRange(0, tileSubSet.Num() - 1)];
	shop->roomType = Room::Minigames;
	shop->obstacleID = 1;
}