// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "FNGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class FNGAME_API UFNGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level")
		int32 level;
	
	
};
