// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "FNGame.h"
#include "FNGameGameMode.h"
#include "FNGamePlayerController.h"
#include "FNGameCharacter.h"
#include "Runtime/Core/Public/Misc/ConfigCacheIni.h"

AFNGameGameMode::AFNGameGameMode()
{
	// use our custom PlayerController class
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_FNGamePlayerController"));
	if (PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
	//PlayerControllerClass = AFNGamePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	FConfigCacheIni::InitializeConfigSystem();
}