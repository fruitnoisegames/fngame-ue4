// Fill out your copyright notice in the Description page of Project Settings.

#include "FNGame.h"
#include "AISpawner.h"
#include "CharacterStats.h"
#include "GenerateMap.h"
#include "WallBase.h"
#include "RoomObstacle.h"
#include "TileRoom.h"


// Sets default values
ATileRoom::ATileRoom()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	bounds = CreateDefaultSubobject<UBoxComponent>(TEXT("Bounds"));
	RootComponent = bounds;
	bounds->SetCollisionProfileName(TEXT("Bounds"));
	bounds->OnComponentBeginOverlap.AddDynamic(this, &ATileRoom::ComponentOverlap);
	source = NULL;
	areDoorsClosed = false;
}

// Called when the game starts or when spawned
void ATileRoom::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATileRoom::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void ATileRoom::CacheAIToSpawn(FAISpawnInfo ai)
{
	aiToSpawn.Add(ai);
}

void ATileRoom::ComponentOverlap_Implementation(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	ACharacterStats *character = Cast<ACharacterStats>(OtherActor);
	if (!character) return;
	else
	{
		if (character->currentRoom != this)
		{
			character->CharacterChangeRoom(character);
		}
		character->previousRoom = character->currentRoom;
		character->currentRoom = this;
	}
}

