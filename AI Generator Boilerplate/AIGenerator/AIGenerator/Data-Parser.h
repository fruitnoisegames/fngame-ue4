#pragma once

#include<iostream>
#include<string>
#include<vector>
#include<fstream>

using namespace std;

struct AIStruct {
	public:
		string name = "";
		float max_hp = 0.0;
		float attack_power = 0.0;
		float defense = 0.0;
		float attack_speed = 0.0;
		float movement_speed = 0.0;
		float difficulty = 1.0;
};

class DataParser {

	public:
		DataParser(string filename);
		vector<AIStruct> ai_data;
		vector<AIStruct> spawn_data;
	private:

};