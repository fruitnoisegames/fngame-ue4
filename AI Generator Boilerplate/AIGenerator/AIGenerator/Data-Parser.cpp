#include "stdafx.h"
#include "Data-Parser.h"
#pragma once


DataParser::DataParser(string filename)
{
	ifstream stream;
	stream.open(filename);

	if (!stream.is_open()) {
		return;
	}

	string content = "";
	int index = 0;
	while (!stream.eof())
	{
		getline(stream, content);
		if (index > 0) {
		
			string parse = "";
			int struct_index = 0;
			AIStruct obj;
			int char_index = 0;
			for (char p : content)
			{
				if (p == ',') {
					switch (struct_index)
					{
						case 0:
							obj.name = parse;
							break;
						case 1:
							obj.max_hp = atof(parse.c_str());
							break;
						case 2:
							obj.attack_power = atof(parse.c_str());
							break;
						case 3:
							obj.defense = atof(parse.c_str());
							break;
						case 4:
							obj.attack_speed = atof(parse.c_str());
							break;
						case 5:
							obj.movement_speed = atof(parse.c_str());
							break;
						default:
							break;
					}
					
					struct_index++;
					parse = "";
				}
				else
				{
					parse += p;
				}
				char_index++;
				if (char_index >= (content.length() - 1))
				{
					obj.movement_speed = atof(parse.c_str());
				}
			}
			ai_data.push_back(obj);
		}
		index++;
	}
}
