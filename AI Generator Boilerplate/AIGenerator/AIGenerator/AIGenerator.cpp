// AIGenerator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Data-Parser.h"
#include <iostream>
#include <vector>

using namespace std;

/*
This function contents is what will go into the AI spawn code in the Unreal Engine.
This function assumes spawning ONE room only.
*/
void GenerateAI(vector<AIStruct> ai_data, vector<AIStruct> &spawn_data) {
	
	int number_of_enemies = 3;
	float difficulty = 1.0;

	if (rand() % 2 == 0) {
		number_of_enemies += rand() % 3;
	}
	else
	{
		number_of_enemies -= rand() % 3;
	}

	difficulty += rand() % 3;

	difficulty -= rand() % 3;

	if (difficulty <= 0.0) {
		difficulty = 1.0;
	}

	for (int k = 0; k < number_of_enemies; k++) {
		AIStruct ai = ai_data.at(rand() % ai_data.size());
		ai.difficulty = difficulty;

		spawn_data.push_back(ai);
	}

}

void PrintContents(vector<AIStruct> output_data) {

	cout << "This room will spawn the following AI with the following stats: " << endl << endl;

	for (AIStruct ai : output_data) {
		cout << "Name: " << ai.name << endl;
		cout.precision(4);
		cout << "Max HP: " << ai.max_hp * ai.difficulty << endl;
		cout << "Attack Power: " << ai.attack_power * ai.difficulty << endl;
		cout << "Defense: " << ai.defense * ai.difficulty << endl;
		cout << "Attack Speed: " << ai.attack_speed * ai.difficulty << endl;
		cout << "Movement Speed: " << ai.movement_speed * ai.difficulty << endl;
		cout << "Difficulty: " << ai.difficulty << endl << endl;

	}
	cout << "----------------------------------------------------------------------" << endl;
}

void AnalyzePool(vector<AIStruct> ai_to_analyze)
{
	float highest = 0.0;
	for (AIStruct ai : ai_to_analyze) {
		if (ai.difficulty >= highest)
		{
			highest = ai.difficulty;
		}
	}
	cout << "Highest difficulty: " << highest << endl;
}


int main()
{

	DataParser *parser = new DataParser("AI Data.csv");
	vector<AIStruct> generated_list;
	vector<AIStruct> analyze_list;

	while (true) {

		int selection;
		cout << "Select an option: 2 - Get statistics of what spawned X times. 1 - Generate a random set of AI. 0 - Exit Program: ";
		cin >> selection;
		cout << endl;

		switch (selection)
		{
			case 1:
				generated_list.clear();
				GenerateAI(parser->ai_data,generated_list);
				PrintContents(generated_list);
				break;
			case 2:
			{
				int iterations = 0;
				cout << "How many spawns do you want stats for? ";
				cin >> iterations;
				analyze_list.clear();
				for (int v = 0; v < iterations; v++)
				{
					generated_list.clear();
					GenerateAI(parser->ai_data, generated_list);
					for (AIStruct s : generated_list){ analyze_list.push_back(s);}
				}
				AnalyzePool(analyze_list);
				break;
			}	
			default:
				return 0;
		}
	}
}

